/*************************************
*     Gone With The Wind
*         首页飘动图片
*          2020-8-11
*************************************/

var xPos = 20;       // 20，起始位置
var yPos = 0.1*document.body.clientHeight;   //document.body.clientHeight  ，起始位置
var step = 1; 
var delay = 30;           //浮动速度,越大越慢
var height = 0; 
var Hoffset = 0; 
var Woffset = 0; 
var yon = 0; 
var xon = 0; 
var pause = true; 
var interval; 
img.style.top = yPos; 
function changePos() { 
    width = document.body.clientWidth; 
    height = document.body.clientHeight; 
    Hoffset = img.offsetHeight; 
    Woffset = img.offsetWidth; 
    img.style.left = xPos + document.body.scrollLeft; 
    img.style.top = yPos + document.body.scrollTop; 
    
    if (yon) { 
        yPos = yPos + step; 
    } 
    else { 
        yPos = yPos - step; 
    } 
    if (yPos < 0) { 
        yon = 1; 
        yPos = 0; 
    } 
    if (yPos >= (height - Hoffset)) { 
        yon = 0; 
        yPos = (height - Hoffset); 
    } 
    if (xon) { 
        xPos = xPos + step; 
    } 
    else { 
        xPos = xPos - step; 
    } 
    if (xPos < 0) { 
        xon = 1; 
        xPos = 0; 
    } 
    if (xPos >= (width - Woffset)) { 
        xon = 0; 
        xPos = (width - Woffset); 
    } 
} 
function start() { 
    img.style.visibility = "visible"; 
    interval = setInterval('changePos()', delay); 
} 

start(); 
